package kingiis.cn.demo

import android.app.Application
import kingiis.cn.demo.util.Okhttp
import okhttp3.OkHttpClient

/**
 * Created by Android研发 on 2018/9/4.
 *
 */
class BaseApplication : Application() {


    @Override
    override fun onCreate() {
        super.onCreate()
    }

    @Override
    override fun onTerminate() {
        super.onTerminate()
    }
}
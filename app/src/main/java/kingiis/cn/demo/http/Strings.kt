package kingiis.cn.demo.http

/**
 * Created by Android研发 on 2018/9/4.
 */
object Strings {
    //数据库名称
    const val NAME = "HOUSE"
    //数据库版本号
    const val VERSION = 1

    public val DATABASE_NAME = "HOUSE"
    public val DATABASE_VERSION = "1"
}
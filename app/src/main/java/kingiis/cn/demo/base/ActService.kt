package kingiis.cn.demo.base

/**
 * Created by Android研发 on 2018/9/4.
 *
 */
interface ActService {
    /**
     * 初始化监听
     */
    fun initListener()

    /**
     * 初始化界面
     */
    fun initView()

    /**
     * 加载界面
     */
    fun getLayout(): Int

    /**
     * 请求失败
     */
    fun onHttpError(url: String, error: String)

    /**
     * 请求成功
     * @param url 接口
     * @param json 数据字符串
     */
    fun onHttpRespone(url: String, json: String)

}
package kingiis.cn.demo.base

import android.app.Activity
import android.os.Bundle
import kingiis.cn.demo.util.Okhttp
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException

/**
 * Created by Android研发 on 2018/9/4.
 * 基类
 */
abstract class BaseAct : Activity(), ActService {

    public val context = this
    private var isFinish = false

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayout())

        initListener()
        initView()
    }

    @Override
    override fun onDestroy() {
        isFinish = true
        super.onDestroy()
    }


    fun getData(url: String, datas: Map<String, String>) {
        Okhttp.post(url, datas, object : Callback {
            override fun onFailure(call: Call?, e: IOException?) {
                if (isFinish) {
                    return
                }
                onHttpError(url, e.toString())
            }

            override fun onResponse(call: Call?, response: Response?) {
                if (isFinish) {
                    return
                }
                onHttpRespone(url, response!!.body()!!.string())
            }
        })
    }
}
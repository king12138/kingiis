package kingiis.cn.demo.util

import kingiis.cn.demo.http.IP
import okhttp3.*
import java.io.IOException

/**
 * Created by Android研发 on 2018/9/4.
 *
 */
object
Okhttp {

    val client = OkHttpClient()

    /**
     * @param  url 接口
     * @param form 键值map集
     * @param callback 回调
     */
    fun post(url: String, form: Map<String, String>, callback: Callback) {
        //临时数集
        val tempForm = form
        val body = FormBody.Builder()
        //请求体
        val request = Request.Builder().url(IP.IP + url)
        //循环map键集
        for (key in tempForm.keys) {
            //值
            val value = form.getValue(key)
            body.add(key, value)
        }
        //方法体准备好了
        //请求
        client.newCall(
                request.post(body.build()
                ).build())
                .enqueue(callback)
    }

}
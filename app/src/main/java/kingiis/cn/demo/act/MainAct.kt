package kingiis.cn.demo.act

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import kingiis.cn.demo.R
import kingiis.cn.demo.base.BaseAct
import kingiis.cn.demo.http.IP
import kotlinx.android.synthetic.main.act_main.*

/**
 * Created by Android研发 on 2018/9/4.
 *  首页
 */
class MainAct : BaseAct() {


    override fun initListener() {
    }

    override fun initView() {
        val map = HashMap<String, String>()
        getData(IP.GET_RANK, map)


        str_h1.text = "helloWorld"

    }

    override fun getLayout(): Int {
        return R.layout.act_main
    }

    override fun onHttpError(url: String, error: String) {

    }

    override fun onHttpRespone(url: String, json: String) {
        Log.i("tag", "json|" + json)
    }

}